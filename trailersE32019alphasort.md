# Videos E3 2019 (ordre alphabétique)

Merci à @netsabes pour le modèle de projet git et de présentation

[Planning des conférences](https://gitlab.com/Photogratte/e3-2019/blob/master/Conf%C3%A9rences_E3_2019.md) | [URLs E3 2019](https://gitlab.com/Photogratte/e3-2019/blob/master/urlsE32019.md) | Une question, une remarque, un truc que j'ai oublié ? DM [@Photogratte](https://twitter.com/Photogratte)

Accès rapide : [#A](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#a) 
[#B](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#b)
[#C](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#c)
[#D](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#d)
[#E](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#e)
[#F](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#f)
[#G](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#g)
[#H](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#h)
[#I](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#i)
[#J](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#j)
[#K](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#k)
[#L](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#l)
[#M](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#m)
[#N](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#n)
[#O](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#o)
[#P](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#p)
[#Q](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#q)
[#R](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#r)
[#S](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#s)
[#T](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#t)
[#U](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#u)
[#V](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#v)
[#W](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#w)
[#X](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#x)
[#Y](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#y)
[#Z](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#z)
[#0-9](https://gitlab.com/Photogratte/e3-2019/blob/master/trailersE32019alphasort.md#0-9)

## Conférences
* Google Stadia https://youtu.be/k-BbW6zAjL0
* Bungie https://www.twitch.tv/bungie
* EA Play https://www.twitch.tv/ea
* Xbox https://www.youtube.com/user/xbox
* Bethesda https://www.twitch.tv/bethesda
* Devolver https://www.twitch.tv/devolverdigital
* UploadVR https://www.twitch.tv/uploadvr
* PC Gaming Show https://www.twitch.tv/pcgamer
* Limited Run Games https://www.twitch.tv/limitedrungames
* Ubisoft https://www.youtube.com/channel/UCEl915e-AtoJ7i1m_SXekTw
* AMD Next Horizon Gaming https://www.youtube.com/user/amd
* Kinda Funny Games Showcase https://www.youtube.com/user/KindaFunnyGames
* Square Enix https://www.youtube.com/user/SquareEnixFrance
* Nintendo https://www.youtube.com/user/NintendoFR 

## Streams variés
* 

## A
* ANIMAL CROSSING (Nintendo)

## B

## C
* Cyberpunk 2077 (CD Projekt Red)

## D
* Darksiders 2019 (THQ Nordic)
* Destroy All Human (THQ Nordic)
* Dragon Ball Project Z (Bandai Namco)

## E

## F

## G
* Ghostbusters: The Video Game Remastered https://www.youtube.com/watch?v=M6ON61SZs-A
* Gears 5 (Microsoft)

## H
* Halo Infinite (Microsoft)

## I

## J

## K

## L

## M

## N

## O
* Outriders (Square Enix)

## P
* POKÉMON EPÉE ET BOUCLIER (Nintendo)

## Q

## R
* Red Faction Evolution (THQ Nordic)

## S
* Star Wars Jedi : The Fallen Order (EA) https://youtu.be/0GLbwkfhYZk

## T
* The Avengers project (Square Enix/Marvel) https://youtu.be/rq_V6WHSmQs

## U

## V

## W
* Watchdog Legion (Ubisoft) https://twitter.com/gameone/status/1136181107588829185

## X

## Y

## Z

## 0-9
